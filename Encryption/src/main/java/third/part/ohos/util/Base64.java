package third.part.ohos.util;

import java.io.UnsupportedEncodingException;

/**
 * 用于对的Base64表示进行编码和解码的实用程序二进制数据.  See RFCs
 * <a href="http://www.ietf.org/rfc/rfc2045.txt">2045</a> and
 * <a href="http://www.ietf.org/rfc/rfc3548.txt">3548</a>.
 */
public class Base64 {

    /**
     * 编码器/解码器标志的默认值.
     */
    public static final int DEFAULT = 0;

    /**
     * 编码器标志位，以省略结尾处的填充“=”字符输出的（如有）.
     */
    public static final int NO_PADDING = 1;

    /**
     * 编码器标志位的一部分，以省略所有行终止符（即输出将在一条长线上）.
     */
    private static final int NO_WRAP = 2;

    /**
     编码器标志位，用于指示行应以CRLF对而不是一个LF。如果也指定了
     */
    private static final int CRLF = 4;

    /**
     编码器/解码器标志位，用于使用“URL和
     文件名安全“Base64的变体（参见RFC 3548第4节），其中
     {@code-}和{@code}被用来代替{@code+}和{@code/}.
     */
    private static final int URL_SAFE = 8;



    private static abstract class Coder {
        byte[] output;
        int op;

    }


    /**
     * 对输入中的Base64编码数据进行解码，并以新的字节数组返回数据.
     *
     * <p>结尾的填充“=”字符被认为是可选的，但如果存在任何字符，则必须有正确的数字。
     *
     * @param str    要解码的输入字符串，使用默认字符集将其转换为字节
     * @param flags  控制解码输出的某些特性。传递{@code DEFAULT}以解码标准Base64
     *
     * @throws IllegalArgumentException 如果输入包含不正确的填充
     */
    public static byte[] decode(String str, int flags) {
        return decode(str.getBytes(), flags);
    }

    /**
     * 对输入中的Base64编码数据进行解码，并以新的字节数组返回数据.
     *
     * <p>结尾的填充“=”字符被认为是可选的，但如果存在任何字符，则必须有正确的数字.
     *
     * @param input 要解码的输入数组
     * @param flags  控制解码输出过程{@code DEFAULT}的某些特性以解码标准Base64
     *
     * @throws IllegalArgumentException 如果输入包含不正确的填充
     */
    private static byte[] decode(byte[] input, int flags) {
        return decode(input, 0, input.length, flags);
    }

    /**
     * 解码输入中的Base64编码数据并返回一个新的字节数组.
     *
     * <p>结尾的填充“=”字符被认为是可选的，但如果存在任何字符，则必须有正确的数字
     *
     * @param input  要解码的数据
     * @param offset 输入数组中开始的位置
     * @param len    要解码的输入字节数
     * @param flags  控制解码输出的某些特性。传递{@code DEFAULT}以解码标准Base64
     *
     * @throws IllegalArgumentException 如果输入包含不正确的填充
     */
    private static byte[] decode(byte[] input, int offset, int len, int flags) {
        // 为输入可以表示的最多数据分配空间.
        // （如果包含空格等，则可能包含较少的内容）
        Decoder decoder = new Decoder(flags, new byte[len*3/4]);

        if (!decoder.process(input, offset, len, true)) {
            throw new IllegalArgumentException("bad base-64");
        }

        // 也许我们运气好，分配了足够的输出空间.
        if (decoder.op == decoder.output.length) {
            return decoder.output;
        }

        // 需要缩短数组，所以分配一个大小合适的新数组并进行复制
        byte[] temp = new byte[decoder.op];
        System.arraycopy(decoder.output, 0, temp, 0, decoder.op);
        return temp;
    }

    private static class Decoder extends Coder {
        /**
         * 用于将字节转换为其在Base64字母表中的位置的查找表.
         */
        private static final int DECODE[] = {
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1,
                -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
                -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        };

        /**
         * 解码“网络安全”变体的查找表（RFC 3548秒。4） 其中-和uu替换+和/。
         */
        private static final int DECODE_WEBSAFE[] = {
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1,
                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1,
                -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63,
                -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        };

        /** 解码数组中的非数据值. */
        private static final int SKIP = -1;
        private static final int EQUALS = -2;

        /**
         * 状态0-3正在读取下一个输入元组
         * 状态4正在读取一个“=”并期望
         * 状态5不希望输入中有更多的数据或填充字符.
         * 状态6是错误状态；在输入中检测到错误，并且没有未来的输入可以“修复”它.
         */
        private int state;
        private int value;

        final private int[] alphabet;

        Decoder(int flags, byte[] output) {
            this.output = output;

            alphabet = ((flags & URL_SAFE) == 0) ? DECODE : DECODE_WEBSAFE;
            state = 0;
            value = 0;
        }

        /**
         * 解码另一块输入数据.
         *
         * @return 如果状态机仍然正常，则为true。如果在输入strea中检测到错误的base-64数据，则为false.
         */
        boolean process(byte[] input, int offset, int len, boolean finish) {
            if (this.state == 6) return false;

            int p = offset;
            len += offset;

            // 使用局部变量使解码器大约有12%
            // 比操作循环中的成员变量更快。（即使是字母表也可以测量差异，因为成员变量是final)
            int state = this.state;
            int value = this.value;
            int op = 0;
            final byte[] output = this.output;
            final int[] alphabet = this.alphabet;

            while (p < len) {
                if (state == 0) {
                    while (p+4 <= len &&
                            (value = ((alphabet[input[p] & 0xff] << 18) |
                                    (alphabet[input[p+1] & 0xff] << 12) |
                                    (alphabet[input[p+2] & 0xff] << 6) |
                                    (alphabet[input[p+3] & 0xff]))) >= 0) {
                        output[op+2] = (byte) value;
                        output[op+1] = (byte) (value >> 8);
                        output[op] = (byte) (value >> 16);
                        op += 3;
                        p += 4;
                    }
                    if (p >= len) break;
                }

                // 快速路径不可用——要么我们读取了部分元组，要么接下来的四个输入字节不全是数据，或者诸如此类。退回到较慢的状态机实现.

                int d = alphabet[input[p++] & 0xff];

                switch (state) {
                    case 0:
                        if (d >= 0) {
                            value = d;
                            ++state;
                        } else if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;

                    case 1:
                        if (d >= 0) {
                            value = (value << 6) | d;
                            ++state;
                        } else if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;

                    case 2:
                        if (d >= 0) {
                            value = (value << 6) | d;
                            ++state;
                        } else if (d == EQUALS) {
                            // 发出最后一个（部分）输出元组;
                            // 只需要一个填充字符.
                            output[op++] = (byte) (value >> 4);
                            state = 4;
                        } else if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;

                    case 3:
                        if (d >= 0) {
                            // 发出输出三元组并返回到状态0.
                            value = (value << 6) | d;
                            output[op+2] = (byte) value;
                            output[op+1] = (byte) (value >> 8);
                            output[op] = (byte) (value >> 16);
                            op += 3;
                            state = 0;
                        } else if (d == EQUALS) {
                            // 发出最后一个（部分）输出元组;
                            // 不需要更多的数据或填充字符.
                            output[op+1] = (byte) (value >> 2);
                            output[op] = (byte) (value >> 10);
                            op += 2;
                            state = 5;
                        } else if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;

                    case 4:
                        if (d == EQUALS) {
                            ++state;
                        } else if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;

                    case 5:
                        if (d != SKIP) {
                            this.state = 6;
                            return false;
                        }
                        break;
                }
            }

            if (!finish) {
                // 已经没有消息了，但电话可能会提供更多信息.
                this.state = state;
                this.value = value;
                this.op = op;
                return true;
            }

            // 完成读取输入。现在找出我们在状态机中的位置并完成.

            switch (state) {
                case 0:
                    // 输出长度是三的倍数.
                    break;
                case 1:
                    // 读取一个额外的输入字节，这不足以生成另一个输出字节 非法.
                    this.state = 6;
                    return false;
                case 2:
                    // 读取两个额外的输入字节，足以再发出一个输出字节.
                    output[op++] = (byte) (value >> 4);
                    break;
                case 3:
                    // 读取三个额外的输入字节，足以再发出两个输出字节
                    output[op++] = (byte) (value >> 10);
                    output[op++] = (byte) (value >> 2);
                    break;
                case 4:
                    // 当我们期望2时，读取一个填充“=”.
                    this.state = 6;
                    return false;
                case 5:
                    // 阅读所有的填充“=”的我们预期的，没有更多.
                    break;
            }

            this.state = state;
            this.op = op;
            return true;
        }
    }


    /**
     * Base64对给定的数据进行编码，并返回一个新分配的字符串和结果.
     *
     * @param input  要编码的数据
     * @param flags  控制编码输出的某些特性。传递{@code DEFAULT}会导致输出符合RFC 2045.
     */
    public static String encodeToString(byte[] input, int flags) {
        try {
            return new String(encode(input, flags), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            // 保证提供US-ASCII。
            throw new AssertionError(e);
        }
    }

    /**
     * Base64对给定的数据进行编码，并返回一个新分配的字节数组和结果.
     *
     * @param input  要编码的数据
     * @param flags  控制编码输出的某些特性。传递{@code DEFAULT}会导致输出符合RFC 2045.
     */
    private static byte[] encode(byte[] input, int flags) {
        return encode(input, 0, input.length, flags);
    }

    /**
     * Base64对给定的数据进行编码，并返回一个新分配的字节数组和结果.
     *
     * @param input  要编码的数据
     * @param offset 输入数组中开始的位置
     * @param len    要编码的输入字节数
     * @param flags  控制编码输出的某些特性。传递{@code DEFAULT}会导致输出符合RFC 2045.
     */
    private static byte[] encode(byte[] input, int offset, int len, int flags) {
        Encoder encoder = new Encoder(flags, null);

        // 计算我们将生成的数组的确切长度.
        int output_len = len / 3 * 4;

        // 说明数据的尾部和填充字节.
        if (encoder.do_padding) {
            if (len % 3 > 0) {
                output_len += 4;
            }
        } else {
            switch (len % 3) {
                case 0: break;
                case 1: output_len += 2; break;
                case 2: output_len += 3; break;
            }
        }

        // 如果有新线则说明.
        if (encoder.do_newline && len > 0) {
            output_len += (((len-1) / (3 * Encoder.LINE_GROUPS)) + 1) *
                    (encoder.do_cr ? 2 : 1);
        }

        encoder.output = new byte[output_len];
        encoder.process(input, offset, len, true);

        assert encoder.op == output_len;

        return encoder.output;
    }

    private static class Encoder extends Coder {
        /**
         * 每隔这么多输出元组发出一个新行。对应于76个字符的行长度（根据
         * <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045</a>).
         */
        static final int LINE_GROUPS = 19;

        /**
         * 将Base64字母表位置（6位）转换为输出字节的查找表.
         */
        private static final byte ENCODE[] = {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
        };

        /**
         * 用于将Base64字母表位置（6位）转换为输出字节的查找表.
         */
        private static final byte ENCODE_WEBSAFE[] = {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_',
        };

        final private byte[] tail;
        /* package */ int tailLen;
        private int count;

        final boolean do_padding;
        final boolean do_newline;
        final boolean do_cr;
        final private byte[] alphabet;

        Encoder(int flags, byte[] output) {
            this.output = output;

            do_padding = (flags & NO_PADDING) == 0;
            do_newline = (flags & NO_WRAP) == 0;
            do_cr = (flags & CRLF) != 0;
            alphabet = ((flags & URL_SAFE) == 0) ? ENCODE : ENCODE_WEBSAFE;

            tail = new byte[2];
            tailLen = 0;

            count = do_newline ? LINE_GROUPS : -1;
        }

        void process(byte[] input, int offset, int len, boolean finish) {
            // 使用局部变量可以使编码器的速度提高.
            final byte[] alphabet = this.alphabet;
            final byte[] output = this.output;
            int op = 0;
            int count = this.count;

            int p = offset;
            len += offset;
            int v = -1;

            // 首先，我们需要将前一个调用的尾部与现在可用的任何输入字节连接起来，看看是否可以清空尾部.

            switch (tailLen) {
                case 0:
                    break;

                case 1:
                    if (p+2 <= len) {
                        // 一个1字节的尾部至少有2个字节的输入.
                        v = ((tail[0] & 0xff) << 16) |
                                ((input[p++] & 0xff) << 8) |
                                (input[p++] & 0xff);
                        tailLen = 0;
                    }
                    break;

                case 2:
                    if (p+1 <= len) {
                        // 至少有1字节输入的2字节尾.
                        v = ((tail[0] & 0xff) << 16) |
                                ((tail[1] & 0xff) << 8) |
                                (input[p++] & 0xff);
                        tailLen = 0;
                    }
                    break;
            }

            if (v != -1) {
                output[op++] = alphabet[(v >> 18) & 0x3f];
                output[op++] = alphabet[(v >> 12) & 0x3f];
                output[op++] = alphabet[(v >> 6) & 0x3f];
                output[op++] = alphabet[v & 0x3f];
                if (--count == 0) {
                    if (do_cr) output[op++] = '\r';
                    output[op++] = '\n';
                    count = LINE_GROUPS;
                }
            }

            // 此时要么没有尾部，要么可用的输入少于3字节.

            // 主循环，在每次迭代中将3个输入字节转换为4个输出字节.
            while (p+3 <= len) {
                v = ((input[p] & 0xff) << 16) |
                        ((input[p+1] & 0xff) << 8) |
                        (input[p+2] & 0xff);
                output[op] = alphabet[(v >> 18) & 0x3f];
                output[op+1] = alphabet[(v >> 12) & 0x3f];
                output[op+2] = alphabet[(v >> 6) & 0x3f];
                output[op+3] = alphabet[v & 0x3f];
                p += 3;
                op += 4;
                if (--count == 0) {
                    if (do_cr) output[op++] = '\r';
                    output[op++] = '\n';
                    count = LINE_GROUPS;
                }
            }

            if (finish) {
                // 完成输入的尾部。请注意，我们需要在输入中剩余的任何字节之前使用尾部的任何字节；总共最多应该有两个字节.

                if (p-tailLen == len-1) {
                    int t = 0;
                    v = ((tailLen > 0 ? tail[t++] : input[p++]) & 0xff) << 4;
                    tailLen -= t;
                    output[op++] = alphabet[(v >> 6) & 0x3f];
                    output[op++] = alphabet[v & 0x3f];
                    if (do_padding) {
                        output[op++] = '=';
                        output[op++] = '=';
                    }
                    if (do_newline) {
                        if (do_cr) output[op++] = '\r';
                        output[op++] = '\n';
                    }
                } else if (p-tailLen == len-2) {
                    int t = 0;
                    v = (((tailLen > 1 ? tail[t++] : input[p++]) & 0xff) << 10) |
                            (((tailLen > 0 ? tail[t++] : input[p++]) & 0xff) << 2);
                    tailLen -= t;
                    output[op++] = alphabet[(v >> 12) & 0x3f];
                    output[op++] = alphabet[(v >> 6) & 0x3f];
                    output[op++] = alphabet[v & 0x3f];
                    if (do_padding) {
                        output[op++] = '=';
                    }
                    if (do_newline) {
                        if (do_cr) output[op++] = '\r';
                        output[op++] = '\n';
                    }
                } else if (do_newline && op > 0 && count != LINE_GROUPS) {
                    if (do_cr) output[op++] = '\r';
                    output[op++] = '\n';
                }

                assert tailLen == 0;
                assert p == len;
            } else {
                // 在tail中保存剩菜，以便下次调用encodeInternal时使用.

                if (p == len-1) {
                    tail[tailLen++] = input[p];
                } else if (p == len-2) {
                    tail[tailLen++] = input[p];
                    tail[tailLen++] = input[p+1];
                }
            }

            this.op = op;
            this.count = count;
        }
    }

    private Base64() { }   // 不实例化

}