# Encryption

#### 项目介绍

Encryption是一个加密工具类，用于加密解密字符串,

#### 安装教程

方式一：
1. 下载jar包[Encryption.jar](https://gitee.com/archermind-ti/Encryption/releases/v1.0.1)。
2. 启动 DevEco Studio，将下载的jar包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

在sdk4，DevEco Studio2.1 beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

方式二：
```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
	...
	implementation 'com.gitee.archermind-ti:Encryption:1.0.1'
	...
}
```

#### 使用说明

1. 获取加密实例
``` java
String key = "YourKey";
String salt = "YourSalt";
byte[] iv = new byte[16];
Encryption encryption = Encryption.getDefault(key, salt, iv);
```

2. 加密您的文本
``` java
String encrypted = encryption.encryptOrNull("Text to be encrypt");
```
3. 解密你的文本
``` java
String decrypted = encryption.decryptOrNull(encrypted);
```

##### 自定义用法
您可以使用自己的构建器  

   ```
     encryption = new Encryption.Builder()
                               .setKeyLength(128)
                               .setKeyAlgorithm("AES")
                               .setCharsetName("UTF8")
                               .setIterationCount(65536)
                               .setKey("mor€Z€cr€tKYss")
                               .setDigestAlgorithm("SHA1")
                               .setSalt("A beautiful salt")
                               .setBase64Mode(Base64.DEFAULT)
                               .setAlgorithm("AES/CBC/PKCS5Padding")
                               .setSecureRandomAlgorithm("SHA1PRNG")
                               .setSecretKeyType("PBKDF2WithHmacSHA1")
                               .setIv(new byte[] { 29, 88, -79, -101, -108, -38, -126, 90, 52, 101, -35, 114, 12, -48, -66, -30 })
                               .build();
   ```

#### 样例

<img src="/encryp.gif" width="33%;" />

#### 版本迭代


- v1.0.1

#### 许可信息
- Copyright (C) 2010 The Android Open Source Project, applied to:
  - Base64 (third.part.android.util.Base64) original comes from [here](https://github.com/android/platform_frameworks_base/blob/ab69e29c1927bdc6143324eba5ccd78f7c43128d/core/java/android/util/Base64.java)

