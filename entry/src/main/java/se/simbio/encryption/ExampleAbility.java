package se.simbio.encryption;

import se.simbio.encryption.slice.ExampleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ExampleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ExampleAbilitySlice.class.getName());
    }
}
