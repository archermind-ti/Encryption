package se.simbio.encryption.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;
import se.simbio.encryption.Encryption;
import se.simbio.encryption.ExampleAbility;
import se.simbio.encryption.ResourceTable;

public class ExampleAbilitySlice extends AbilitySlice implements Component.ClickedListener {


    Button mpwdString;
    Button mdepwdString;
    Button mpwdJump;
    TextField mpwdShow;
    ToastDialog mtoastDialog;
    Encryption mEncryption = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        mpwdString = (Button) findComponentById(ResourceTable.Id_pwdString);
        mdepwdString = (Button) findComponentById(ResourceTable.Id_depwdString);
        mpwdShow = (TextField) findComponentById(ResourceTable.Id_pwdShow);
        mpwdJump = (Button) findComponentById(ResourceTable.Id_pwdJump);
        mpwdString.setClickedListener(this);
        mdepwdString.setClickedListener(this);
        mpwdJump.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        String originWord = mpwdShow.getText();
        String termi = null;
        byte[] ss = new byte[16];

        mtoastDialog = new ToastDialog(this);
        switch (component.getId()) {
            case ResourceTable.Id_pwdString:
                mEncryption = Encryption.getDefault("YourKey", "YourSalt", ss);
                try {
                    termi = mEncryption.encryptOrNull(originWord);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mtoastDialog.setText(termi);
                mpwdShow.setText(termi);
                break;
            case ResourceTable.Id_depwdString:
                mEncryption = Encryption.getDefault("YourKey", "YourSalt", ss);
                try {
                    termi = mEncryption.decryptOrNull(originWord);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mtoastDialog.setText(termi);
                mpwdShow.setText(termi);
                break;
            case ResourceTable.Id_pwdJump:
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName(ExampleAbility.class)
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                break;
            default:
                break;
        }
    }
}
