package se.simbio.encryption;

import org.junit.Assert;
import org.junit.Test;

public class EncryptionTest {

    @Test
    public void testEncryptionAndEncrypted() {

        String text = "Text to be encrypt";

        String key = "YourKey";
        String salt = "YourSalt";
        byte[] iv = new byte[16];
        Encryption encryption = Encryption.getDefault(key, salt, iv);
        String encrypted = encryption.encryptOrNull(text);
        String decrypted = encryption.decryptOrNull(encrypted);

        Assert.assertEquals(decrypted, text);
    }


    @Test
    public void testEncryptionCallback() {

        String text = "Text to be encrypt";

        String key = "YourKey";
        String salt = "YourSalt";
        byte[] iv = new byte[16];
        Encryption encryption = Encryption.getDefault(key, salt, iv);

        encryption.encryptAsync(text, new Encryption.Callback() {
            @Override
            public void onSuccess(String result) {
                encryption.decryptAsync(result, new Encryption.Callback() {
                    @Override
                    public void onSuccess(String result) {
                        Assert.assertEquals(result, text);
                    }

                    @Override
                    public void onError(Exception exception) {

                    }
                });
            }

            @Override
            public void onError(Exception exception) {

            }
        });
    }
}
